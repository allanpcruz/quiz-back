API - Quiz
==================

A API Quiz oferece a funcionalidade de gravação das respostas dos participantes do quiz.

O objetivo principal é avaliar a proficiência do aluno em determinado assunto.

Nesta primeira versão, apenas as respostas serão gravadas. Futuras implementações permitirão gravar também as perguntas, bem como as opções a serem disponibilizadas.

* Método: POST

* Endpoint: /api/quiz

* Parâmetros da Requisição:

    1. nome (String): Nome do aluno;
    2. cidade (String): Cidade do aluno;
    3. perguntas (Object): Objeto com as respostas às perguntas do quiz:
        Exemplo do objeto:
```
            {
                participante: { nome: '', cidade: '' },
                perguntas: {
                    q1: null,
                    q2: null,
                    q3: null,
                    q4: null,
                    q5: null,
                    q6: null,
                    q7: null,
                    q8: null,
                    q9: null,
                    q10: null,
                    q11: null,
                    q12: null,
                    q13: null,
                    q14: null,
                    q15: null
                }
            }
```



* Resposta de Sucesso: Retorna o código 201 indicando que as respostas foram gravadas com sucesso.

* Resposta de Erro: Retorna o código 500 em caso de erro interno.

