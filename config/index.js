require('dotenv/config');

const databaseConfig = {
  development: {
    username: "root",
    password: "root",
    database: "quiz_d",
    host: "127.0.0.1",
    dialect: "mysql"
  },
  test: {
    username: "root",
    password: "root",
    database: "quiz_t",
    host: "127.0.0.1",
    dialect: "mysql"
  },
  production: {
    username: "quiz_u",
    password: "5gxIH5nI0rS1tIzshaSN",
    database: "quiz_p",
    host: "10.100.56.138",
    dialect: "mysql"
  }
};

const config = Object.freeze({
  app_port: process.env.APP_PORT,
  api_version: process.env.API_VERSION,
  ...databaseConfig,
});

module.exports = config;
