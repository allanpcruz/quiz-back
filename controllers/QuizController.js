const { Quiz } = require('../models');

class QuizController {
    static async gravarQuiz(req, res) {
        try {
            console.log("🚀 ~ QuizController ~ gravarQuiz ~ req.body:", req.body);
            const dadosQuiz = req.body;
            const novoQuiz = await Quiz.create({
                nome: dadosQuiz.participante.nome,
                cidade: dadosQuiz.participante.cidade,
                respostas: JSON.stringify(dadosQuiz.perguntas)
            });
            return res.status(201).end();
        } catch (erro) {
            console.error('Erro ao gravar o quiz:', erro);
            return res.status(500).json({ mensagem: 'Erro interno do servidor' });
        }
    }
}

module.exports = QuizController;
