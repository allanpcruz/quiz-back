const express = require('express');
const moment = require('moment');
const bodyParser = require('body-parser');
const config = require('./config');
const routes = require('./routes');
const port = config.app_port;
const cors = require('cors');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors({
    origin: '*',
}));

routes(app);

app.use((_req, res, _next) => {
    res.status(404).json({ msgErro: 'Rota inválida ou inexistente' });
});

const server = app.listen(port, () => {
    console.log(`Servidor iniciado em ${moment().format('DD/MM/YYYY')} às ${moment().format('HH:mm:ss')} na porta ${port}`);
});
