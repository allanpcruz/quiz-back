const bodyParser = require('body-parser');

const quiz = require('./quiz.js');

module.exports = app => {
    app.use(
        bodyParser.json(),
        bodyParser.urlencoded({ extended: true }),
        quiz,
    );
};