const { Router } = require('express');
const QuizController = require('../controllers/QuizController');
const router = Router();

router.post('/api/quiz', QuizController.gravarQuiz);

module.exports = router;
